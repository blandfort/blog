# Blog

Lightweight Python-based content management system for blogs.


## Installation

### Local Setup (for Development)

Initially, from the directory [flask/](flask/) install requirements with `pip install -r requirements.txt`

Then run the blog with `python run.py`. Run `python run.py --help` for information on available arguments.


### Docker Compose

Create a directory `flask/contents/` with the contents of your blog (same files as in [flask/demo_contents/](flask/demo_contents/).

Make sure that docker and docker-compose are installed, and adjust the URLs in [Caddyfile](Caddyfile).

Then run the blog with `docker-compose up --build`.

Note: Even though docker-compose is not recommended for production use,
for small blogs this setup should be sufficient.


## Contents

### Adding a new Post

Create a markdown file in the directory "posts" within your content directory (for the demo directory, this is [flask/demo_contents/posts/](flask/demo_contents/posts/)).

Add metadata for the new post in the posts file "posts.yaml" (for the demo directory, this file is [flask/demo_contents/posts.yaml](flask/demo_contents/posts.yaml)).


### Adding Posts to Footer Menu

In the menus file "menus.yaml" ([flask/demo_contents/menus.yaml](flask/contents/menus.yaml) for the demo directory), add the name of the post to the footer menu list.

For the list of available posts, check the posts file "posts.yaml".


## Style

### Choosing Color Schemes

There are various color schemes available in [flask/templates/color_schemes](flask/templates/color_schemes).
You can choose them by setting the environment variables `COLOR_SCHEME` or
passing the name of the color scheme to `run.py` when calling it over the command line.

Screenshots for some of the themes:

* "light": [blog_light.png](https://drive.google.com/file/d/1jVJQKm1k_ck9yOzoYx3iVQAV6BjOtM0x/view?usp=share_link)
* "dark": [blog_dark.png](https://drive.google.com/file/d/1fAfvj4huNVvUK3sF2Sz4lLNcVxRYpa7N/view?usp=share_link)

### Adding Color Schemes

You can either modify the existing files in the color scheme directory,
or add a new color scheme file there.


## Parsing of Contents

Within posts, the following syntax can be used:

- Refer to other posts with `post[<post_name>]`
- Randomize contents with `?[<option_1>|<option_2>|...]`


## TODO

- Have the feeling that randomization is not that random, check that
- Can we also add a simple bot to announce new posts on twitter?