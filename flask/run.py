import random
import os
import urllib.parse
from flask import Flask
from flask import render_template
from flask import request, flash, redirect, abort, send_file
import datetime

import logger


if __name__=='__main__':
    # NOTE: We need to parse arguments here already, since this sets the content dir
    # variable which has to exist when loading the rest
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('-c', '--content_dir', dest='content_dir',
                        help='Path to the content directory (relative to flask dir)',
                        default='demo_contents/')
    parser.add_argument('-s', '--colors', dest='color_scheme',
                        help='Name of the color scheme (See templates/color_scheme for available options)',
                        default='dark')

    args = parser.parse_args()
    os.environ['CONTENT_DIR'] = args.content_dir

    color_scheme = args.color_scheme
    if color_scheme.endswith('.yaml'):
        color_scheme = color_scheme.rsplit('.', 1)[0]
    os.environ['COLOR_SCHEME'] = color_scheme


from content import Blog, POST_URL_PATTERN

app = Flask(__name__)


def get_html(template_name: str, blog: Blog, **content):
    return render_template(
        template_name,
        menu=[dict(item) for item in blog.menu],
        blog=blog.about,
        colors=blog.colors,
        footer_menu=blog.footer_menu,
        randomize=random.choice,
        **content,
        )

@app.route('/robots.txt')
def robots():
    return send_file(CONTENT_DIR / Path('robots.txt'))

@app.route(POST_URL_PATTERN % '<name>')
def post(name):
    blog = Blog()

    try:
        post = blog.get_post(name=urllib.parse.unquote(name))
    except FileNotFoundError:
        abort(404)

    return get_html('post.html', blog=blog, post=dict(post))

@app.route('/')
@app.route('/index')
def index():
    return post('home')


if __name__=='__main__':
    app.run(host='0.0.0.0', port=8002, debug=True)
