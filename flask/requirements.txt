Flask>=2.1.1
gunicorn>=20.0.4
joblib>=0.17.0
Markdown>=3.1.1
requests>=2.22.0
pydantic
pyyaml
