## Header 1

Some test stuff

Page link: page[imprint]

## Header 2

And more stuff here...

## Random stuff

Random coin toss: ?[Heads|Tails]

Random number between 1 and 6: ?[1-6]

### Nested randomization

Coin or dice?

?[Coin: ?[Heads|Tails]|Dice: ?[1-6]]
