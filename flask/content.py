import logging
import os
import yaml
import markdown
import re
import datetime
import random
import urllib.parse
from yaml.loader import SafeLoader
from pathlib import Path
from pydantic import BaseModel
from typing import List, Optional


log = logging.getLogger(__name__)

CONTENT_DIR = Path(os.getenv('CONTENT_DIR'))
COLOR_SCHEME = os.getenv('COLOR_SCHEME')
POST_URL_PATTERN = '/posts/%s'


def post_link(name: str):
    quoted_name = urllib.parse.quote(name, safe="")
    return POST_URL_PATTERN % quoted_name


class Post(BaseModel):
    title: str
    content: str
    created: Optional[datetime.date] = None
    modified: datetime.date
    category: Optional[str] = None  # If given, expect the post to be in a sub-directory

class MenuItem(BaseModel):
    title: str
    link: str

class Blog:

    def __init__(self, content_dir: Optional[Path] = None, color_scheme: Optional[str] = None):
        if content_dir is None:
            content_dir = CONTENT_DIR
        if color_scheme is None:
            color_scheme = COLOR_SCHEME
        self._load(content_dir=content_dir, color_scheme=color_scheme)

    def _load(self, content_dir: Path, color_scheme: str):
        with open(content_dir / Path('blog.yaml')) as f:
            self.about = yaml.load(f, Loader=SafeLoader)
        with open(Path(f'templates/color_schemes/{color_scheme}.yaml')) as f:
            self.colors = yaml.load(f, Loader=SafeLoader)
        with open(content_dir / Path('posts.yaml')) as f:
            # dict of form {name: Post}
            self.posts = yaml.load(f, Loader=SafeLoader)
        with open(content_dir / Path('menus.yaml')) as f:
            self.menus = yaml.load(f, Loader=SafeLoader)

        self.post_dir = content_dir / Path('posts/')

    @property
    def footer_menu(self):
        return [{'post': post, 'title': self.posts[post]['title']}
            for post in self.menus['footer']]

    def get_recent_posts(self, max_num: int=3):
        post_list = [(name, post) for name, post in self.posts.items()
                    if 'created' in post]
        post_list = sorted(post_list, key=lambda p: p[1]['created'])[::-1][:max_num]

        return post_list

    @property
    def menu(self) -> List[MenuItem]:
        if len(self.posts)>0:
            recent_posts = self.get_recent_posts()

            return [MenuItem(title=post['title'], link=post_link(name))
                    for name, post in recent_posts]
        else:
            #TODO read menu items from some file
            return [
                MenuItem(title='Home', link='/'),
                MenuItem(title='Test', link=post_link('test')),
            ]

    def _parse_content(self, content: str, max_iterations: int=3) -> str:
        previous_content = content[:]

        # We iterate to allow for nested expressions
        for _ in range(max_iterations):
            # Random numbers
            def process_random_number(res):
                r = res.group(1).split('-')
                return str(random.randint(int(r[0]), int(r[1])))

            content = re.sub(r"\?\[(\d+-\d+)\]",
                                lambda x: process_random_number(x), content)

            # Random choice
            def process_random_regex(res):
                options = res.group(1).split('|')
                return random.choice(options)

            content = re.sub(r"\?\[(?P<options>([^\]\[]|[\s]\[([^\]\[])*\])+)\]",
                                lambda x: process_random_regex(x), content)

            # Links to other pages
            def make_link_from_regex(res):
                type_ = res.group(1)
                name = res.group(2)

                if type_=='post' or type_=='page':
                    return f'<a href="{post_link(name)}">{name}</a>'
                else:
                    # Otherwise return the original string
                    return f'{type_}[{name}]'

            content = re.sub(r"(?P<type>[a-zA-Z]+)\[(([^\]\[]|[\s]\[([^\]\[])*\])+)\]",
                                lambda x: make_link_from_regex(x), content)

            if content==previous_content:
                break
            previous_content = content[:]

        return markdown.markdown(content)

    def get_post(self, name: str) -> Post:
        if name in self.posts:
            post = self.posts[name]

            if "category" in post:
                post_path = self.post_dir / post["category"] / Path(f'{name}.md')
            else:
                post_path = self.post_dir / Path(f'{name}.md')

            with open(post_path, 'r') as f:
                content = f.read()
            post['content'] = self._parse_content(content)
            return Post(**post)
        else:
            raise FileNotFoundError("Invalid post!")

